variable "ami_id" {
  description = "The name (no ID) of the AMI to use for the ASG, it accepts a wildcard e.g. `AO-Windows2019-Base-*`"
  type        = string
}

variable "ami_owners" {
  description = "A list of ownmers for the AMI that you want to set for the instnace to use at launch. Valid values are amazon (images produced by AWS), self (images that are hosted in the current account), AWS account numbers (shared & marketplace images)"
  type        = list(string)
  default     = ["self", "amazon"]
}

variable "asg_desired" {
  description = "The default amount of instances to have active in the ASG"
  type        = number
}

variable "asg_max" {
  description = "The maximum amount of instances the ASG can scale up to"
  type        = number
  default     = 1
}

variable "asg_min" {
  description = "The minimum amount of instances the ASG can scale down to"
  type        = number
  default     = 0
}

variable "associate_public_ip_address" {
  description = "If a public IP should be assigned to the instances when they're created. This should only be used if the route to the internet, in the subnets you're deploying to, is an Internet Gateway"
  type        = bool
  default     = false
}

variable "custom_instance_tags" {
  description = "A map of tags that will be appended to the tags applied by the module and applied to the EC2 instances. Any tag supplied that matches the name of a tag set by the module will take precedence"
  type        = map(string)
  default     = {}
}

variable "custom_volume_tags" {
  description = "A map of tags that will be appended to the tags applied by the module and applied to the EC2 instance volumes. Any tag supplied that matches the name of a tag set by the module will take precedence"
  type        = map(string)
  default     = {}
}

variable "detailed_monitoring" {
  description = "Whether detailed monitoring (1 minute vs 5 minute periods) should be enabled for the instances"
  type        = bool
  default     = false
}

variable "ebs_optimized" {
  description = "Turns on EBS optimisation"
  type        = bool
  default     = true
}

variable "envname" {
  description = "The name of the environment being deployed into e.g. beta, staging, live"
  type        = string
}

variable "health_check_grace_period" {
  description = "The grace period before starting health checks"
  default     = 300
}

variable "health_check_type" {
  description = "The type of health checks to perform to determine instance health, set this to ELB if it's behind a load balancer"
  type        = string
  default     = "EC2"
}

variable "iam_instance_profile" {
  description = "The IAM Instance Profile name to assign to the instances"
  type        = string
}

variable "instance_type" {
  description = "The EC2 instance type to provision with. Determines available CPU and RAM"
  type        = string
  default     = "t2.micro"
}

variable "key_name" {
  description = "The AWS EC2 Key Pair to assign the instances"
  type        = string
}

variable "name" {
  description = "The name to give the ASG and it's instances"
  type        = string
}

variable "protect_from_scale_in" {
  description = "Prevent instances from being removed when the ASG attempts to scale in during reduced demand"
  default     = false
}

variable "security_groups" {
  description = "A list of Security Groups to assign to the ASG instances. Must match the VPC `subnets` are in"
  type        = list(string)
}

variable "subnets" {
  description = "A list of subnet IDs to allow the ASG to provision instances in. Must match the VPC `security_groups` are in"
  type        = list(string)
}

variable "target_group_arns" {
  description = "A list of target group ARNs to associate the ASG with"
  type        = list(string)
  default     = []
}

variable "user_data" {
  description = "Content of userdata file"
  type        = string
}

variable "wait_for_capacity_timeout" {
  description = "Determines how long terraform will wait for an ASG to scale up before timing out"
  type        = string
  default     = "10m"
}
