output "asg_arn" {
  description = "*string* The ARN of the Auto Scaling Group"
  value       = aws_autoscaling_group.asg.arn
}

output "asg_id" {
  description = "*string* The ID of the Auto Scaling Group"
  value       = aws_autoscaling_group.asg.id
}

output "asg_name" {
  description = "*string* The name of the ASG"
  value       = aws_autoscaling_group.asg.name
}

output "health_check_grace_period" {
  description = "*number* The grace period before starting health checks"
  value       = var.health_check_grace_period
}

output "launch_template_arn" {
  description = "*string* The ARN of the Launch Template"
  value       = aws_launch_template.lt.arn
}

output "launch_template_default_version" {
  description = "*number* The Launch Templates default version"
  value       = aws_launch_template.lt.default_version
}

output "launch_template_id" {
  description = "*string* The ID of the Launch Template"
  value       = aws_launch_template.lt.id
}

output "launch_template_latest_version" {
  description = "*number* The latest version of the Launch Template, does not have to match the default version"
  value       = aws_launch_template.lt.latest_version
}

output "subnets" {
  description = "*list(string)* The subnet IDs configured for the ASG"
  value       = var.subnets
}

output "wait_for_capacity_timeout" {
  description = "*string* How long terraform will wait for an ASG to scale up before timing out"
  value       = var.wait_for_capacity_timeout
}
