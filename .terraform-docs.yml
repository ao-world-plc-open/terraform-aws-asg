---
content: |-
  # Terraform Module: AWS ASG

  Module for the creation of an AWS Auto Scaling Group and Launch
  Configuration.

  There will be no further development of this module for Terraform 0.11.x.
  Please use v4.0.0 for Terraform 0.12.0 to 0.12.19 If you would still like to
  use this module with Terraform 0.11.x then use v3.1.1.

  * [Example Usage](#example-usage)
    * [Basic](#basic)
  * [Requirements](#requirements)
  * [Inputs](#inputs)
  * [Outputs](#outputs)
  * [Contributing](#contributing)
  * [Change Log](#change-log)
  
  ## Example Usage

  ### Basic

  ```hcl
  {{ include "examples/basic/main.tf" }}
  ```

  {{ .Requirements }}

  {{ .Inputs }}

  {{ .Outputs }}

  ## Contributing

  Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to contribute to changes to this module.

  ## Change Log

  Please see [CHANGELOG.md](CHANGELOG.md) for changes made between different tag versions of the module.

formatter: markdown

header-from: main.tf

output:
  file: README.md
  mode: replace
  # By default this adds a header and footer comment that shows up Bitbucket
  template: |-
    {{ .Content }}

# Can't, and shouldn't, set what values the outputs are at the module level
output-values:
  enabled: false

# Don't descend into the module and create submodule documentation
# It doesn't effect showing module dependencies
recursive:
  enabled: false

settings:
  anchor: false
  default: true
  description: true
  hide-empty: true
  html: false
  indent: 2
  required: true
  sensitive: false
  type: true

sort:
  by: required
  enabled: true

# Pinned to 0.15.X until this bug with indentation is fixed
# https://github.com/terraform-docs/terraform-docs/issues/577
version: ~> v0.15.0
