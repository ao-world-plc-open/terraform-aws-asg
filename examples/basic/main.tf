module "asg" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-asg.git?ref=v5.1.0"

  ami_id               = "AO-Windows2019-Base-*"
  asg_desired          = 1
  envname              = "demo"
  iam_instance_profile = "demo-instance-profile"
  key_name             = "demo-key-pair"
  name                 = "testing"
  security_groups = [
    "security-group-id"
  ]
  subnets = [
    "subnet-id"
  ]
  user_data = "<powershell></powershell>"
}