//Launch Template
resource "aws_launch_template" "lt" {
  ebs_optimized = var.ebs_optimized

  iam_instance_profile {
    name = var.iam_instance_profile
  }

  image_id                             = data.aws_ami.ami.id
  instance_initiated_shutdown_behavior = "terminate"
  instance_type                        = var.instance_type
  key_name                             = var.key_name

  monitoring {
    enabled = var.detailed_monitoring
  }

  network_interfaces {
    delete_on_termination       = true
    associate_public_ip_address = var.associate_public_ip_address
    security_groups             = var.security_groups
  }

  tag_specifications {
    resource_type = "instance"

    tags = merge(
      {
        "Name" = var.name
      },
      {
        "Environment" = var.envname
      },
      var.custom_instance_tags,
    )
  }

  tag_specifications {
    resource_type = "volume"

    tags = merge(
      {
        "Name" = var.name
      },
      {
        "Environment" = var.envname
      },
      var.custom_volume_tags,
    )
  }

  user_data = base64encode(var.user_data)

  tags = merge(
    {
      "Name" = var.name
    },
    {
      "Environment" = var.envname
    },
    var.custom_instance_tags,
  )
}

// Auto-Scaling Group Configuration
resource "aws_autoscaling_group" "asg" {
  name                = var.name
  vpc_zone_identifier = var.subnets

  launch_template {
    id      = aws_launch_template.lt.id
    version = "$Latest"
  }

  target_group_arns         = var.target_group_arns
  min_size                  = var.asg_min
  max_size                  = var.asg_max
  desired_capacity          = var.asg_desired
  wait_for_capacity_timeout = var.wait_for_capacity_timeout
  health_check_grace_period = var.health_check_grace_period
  health_check_type         = var.health_check_type
  protect_from_scale_in     = var.protect_from_scale_in == 0 ? false : true

  dynamic "tag" {
    for_each = local.tags_asg_format

    content {
      key                 = "Name"
      value               = var.name
      propagate_at_launch = true
    }
  }

  dynamic "tag" {
    for_each = local.tags_asg_format

    content {
      key                 = "Environment"
      value               = var.envname
      propagate_at_launch = true
    }
  }
}

