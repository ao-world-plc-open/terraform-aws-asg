# Change Log

## v5.1.0 (07/12/2021)

* Added .terraform-docs.yml to auto generate README.md
* Added .gitlab-ci.yml to enforce fmt and doc regeneration
* Updated CONTRIBUTING.md
* All variables and outputs now have descriptions and types, and have been
  organised alphabetically
* README.md has been refreshed with the auto generated version

## v5.0.0 (28/05/2021)

* Moving to required_providers syntax has now depreciated 0.12
* Added minimum provider version contraints per Terraform best practice
* Confirmed terraform validate passing for terraform 0.13, 0.14, and 0.15
* Minimum aws provider version set to where support for .aws/sso/cache is enabled

## v4.0.1 (28/09/2020)

* fix conditional syntax

## v4.0.0 (14/08/2020)

* Upgrades TF to 12.x with provider 2.64

## v3.1.1 (11/08/2020)

* Additional variable to enable/disable 'protect_from_scale_in' on the ASG.

## v3.1.0 (29/06/2020)

* Additional variable to enable/disable EBS optimisation.

## v3.0.2 (22/06/2020)

* Additional outputs that can be used outside the module.

## v3.0.1 (04/06/2020)

* Fixes an issue where the instances tags were being applied to volumes instead of the volume tags.

## v3.0.0 (04/06/2020)

**BREAKING CHANGE**

* Adds ami_owners as an argument so that shared images can be found.
* Removes the availability_zone variable due to it only being required for EC2 Classic.

## v2.0.1 (29/05/2020)

* Ensures Network Interfaces are removed when the EC2 instance gets terminated.

## v2.0.0 (29/05/2020)

**BREAKING CHANGE**

* Module will now create Launch Templates instead of Launch Configurations
* The variable for custom_tags has been replaced with custom_instance_tags and custom_volume_tags

## v1.0.0 (09/04/2020)

**BREAKING CHANGE**

* Adds custom_tags argument which will take a map of tags
* Removes service & patch_group tag - these can be specified through custom_tags if required

## v0.2.0

* Adds owners argument to AMI lookup as it is a required field
* Adds wait for capacity argument to allow Windows instances time to spin up

## v0.1.0

* Initial Commit
