# Terraform Module: AWS ASG

Module for the creation of an AWS Auto Scaling Group and Launch
Configuration.

There will be no further development of this module for Terraform 0.11.x.
Please use v4.0.0 for Terraform 0.12.0 to 0.12.19 If you would still like to
use this module with Terraform 0.11.x then use v3.1.1.

* [Example Usage](#example-usage)
  * [Basic](#basic)
* [Requirements](#requirements)
* [Inputs](#inputs)
* [Outputs](#outputs)
* [Contributing](#contributing)
* [Change Log](#change-log)

## Example Usage

### Basic

```hcl
module "asg" {
  source = "git::https://gitlab.com/ao-world-plc-open/terraform-aws-asg.git?ref=v5.1.0"

  ami_id               = "AO-Windows2019-Base-*"
  asg_desired          = 1
  envname              = "demo"
  iam_instance_profile = "demo-instance-profile"
  key_name             = "demo-key-pair"
  name                 = "testing"
  security_groups = [
    "security-group-id"
  ]
  subnets = [
    "subnet-id"
  ]
  user_data = "<powershell></powershell>"
}
```

## Requirements

| Name | Version |
|------|---------|
| terraform | >= 0.13.0 |
| aws | >= 3.26.0 |
| null | >= 3.1.0 |

## Inputs

| Name | Description | Type | Default | Required |
|------|-------------|------|---------|:--------:|
| ami\_id | The name (no ID) of the AMI to use for the ASG, it accepts a wildcard e.g. `AO-Windows2019-Base-*` | `string` | n/a | yes |
| asg\_desired | The default amount of instances to have active in the ASG | `number` | n/a | yes |
| envname | The name of the environment being deployed into e.g. beta, staging, live | `string` | n/a | yes |
| iam\_instance\_profile | The IAM Instance Profile name to assign to the instances | `string` | n/a | yes |
| key\_name | The AWS EC2 Key Pair to assign the instances | `string` | n/a | yes |
| name | The name to give the ASG and it's instances | `string` | n/a | yes |
| security\_groups | A list of Security Groups to assign to the ASG instances. Must match the VPC `subnets` are in | `list(string)` | n/a | yes |
| subnets | A list of subnet IDs to allow the ASG to provision instances in. Must match the VPC `security_groups` are in | `list(string)` | n/a | yes |
| user\_data | Content of userdata file | `string` | n/a | yes |
| ami\_owners | A list of ownmers for the AMI that you want to set for the instnace to use at launch. Valid values are amazon (images produced by AWS), self (images that are hosted in the current account), AWS account numbers (shared & marketplace images) | `list(string)` | ```[ "self", "amazon" ]``` | no |
| asg\_max | The maximum amount of instances the ASG can scale up to | `number` | `1` | no |
| asg\_min | The minimum amount of instances the ASG can scale down to | `number` | `0` | no |
| associate\_public\_ip\_address | If a public IP should be assigned to the instances when they're created. This should only be used if the route to the internet, in the subnets you're deploying to, is an Internet Gateway | `bool` | `false` | no |
| custom\_instance\_tags | A map of tags that will be appended to the tags applied by the module and applied to the EC2 instances. Any tag supplied that matches the name of a tag set by the module will take precedence | `map(string)` | `{}` | no |
| custom\_volume\_tags | A map of tags that will be appended to the tags applied by the module and applied to the EC2 instance volumes. Any tag supplied that matches the name of a tag set by the module will take precedence | `map(string)` | `{}` | no |
| detailed\_monitoring | Whether detailed monitoring (1 minute vs 5 minute periods) should be enabled for the instances | `bool` | `false` | no |
| ebs\_optimized | Turns on EBS optimisation | `bool` | `true` | no |
| health\_check\_grace\_period | The grace period before starting health checks | `number` | `300` | no |
| health\_check\_type | The type of health checks to perform to determine instance health, set this to ELB if it's behind a load balancer | `string` | `"EC2"` | no |
| instance\_type | The EC2 instance type to provision with. Determines available CPU and RAM | `string` | `"t2.micro"` | no |
| protect\_from\_scale\_in | Prevent instances from being removed when the ASG attempts to scale in during reduced demand | `bool` | `false` | no |
| target\_group\_arns | A list of target group ARNs to associate the ASG with | `list(string)` | `[]` | no |
| wait\_for\_capacity\_timeout | Determines how long terraform will wait for an ASG to scale up before timing out | `string` | `"10m"` | no |

## Outputs

| Name | Description |
|------|-------------|
| asg\_arn | *string* The ARN of the Auto Scaling Group |
| asg\_id | *string* The ID of the Auto Scaling Group |
| asg\_name | *string* The name of the ASG |
| health\_check\_grace\_period | *number* The grace period before starting health checks |
| launch\_template\_arn | *string* The ARN of the Launch Template |
| launch\_template\_default\_version | *number* The Launch Templates default version |
| launch\_template\_id | *string* The ID of the Launch Template |
| launch\_template\_latest\_version | *number* The latest version of the Launch Template, does not have to match the default version |
| subnets | *list(string)* The subnet IDs configured for the ASG |
| wait\_for\_capacity\_timeout | *string* How long terraform will wait for an ASG to scale up before timing out |

## Contributing

Please see [CONTRIBUTING.md](CONTRIBUTING.md) for details on how to contribute to changes to this module.

## Change Log

Please see [CHANGELOG.md](CHANGELOG.md) for changes made between different tag versions of the module.